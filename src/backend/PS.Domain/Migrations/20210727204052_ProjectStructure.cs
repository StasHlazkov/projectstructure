﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace PS.Domain.DAL.Migrations
{
    public partial class ProjectStructure : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Teams",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Teams", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FirstName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    LastName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Email = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    RegisteredAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    BirthDay = table.Column<DateTime>(type: "datetime2", nullable: false),
                    TeamId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Users_Teams_TeamId",
                        column: x => x.TeamId,
                        principalTable: "Teams",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Projects",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Deadline = table.Column<DateTime>(type: "datetime2", nullable: false),
                    TeamId = table.Column<int>(type: "int", nullable: true),
                    AuthorId = table.Column<int>(type: "int", nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Projects", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Projects_Teams_TeamId",
                        column: x => x.TeamId,
                        principalTable: "Teams",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Projects_Users_AuthorId",
                        column: x => x.AuthorId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Tasks",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Description = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    State = table.Column<int>(type: "int", nullable: false),
                    FinishedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    CreatedAt = table.Column<DateTime>(type: "datetime2", nullable: false),
                    PerformerId = table.Column<int>(type: "int", nullable: true),
                    ProjectId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tasks", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Tasks_Projects_ProjectId",
                        column: x => x.ProjectId,
                        principalTable: "Projects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Tasks_Users_PerformerId",
                        column: x => x.PerformerId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                table: "Teams",
                columns: new[] { "Id", "CreatedAt", "Name" },
                values: new object[,]
                {
                    { 1, new DateTime(2021, 7, 27, 22, 40, 51, 700, DateTimeKind.Local).AddTicks(1084), "Et necessitatibus impedit et." },
                    { 19, new DateTime(2021, 7, 27, 22, 40, 51, 703, DateTimeKind.Local).AddTicks(3114), "Quaerat eum dolor." },
                    { 18, new DateTime(2021, 7, 27, 22, 40, 51, 703, DateTimeKind.Local).AddTicks(3090), "Delectus repellat possimus." },
                    { 17, new DateTime(2021, 7, 27, 22, 40, 51, 703, DateTimeKind.Local).AddTicks(3061), "Quam cupiditate cum ut." },
                    { 16, new DateTime(2021, 7, 27, 22, 40, 51, 703, DateTimeKind.Local).AddTicks(3010), "Nisi eius eum suscipit labore optio sed et dolorem repudiandae." },
                    { 15, new DateTime(2021, 7, 27, 22, 40, 51, 703, DateTimeKind.Local).AddTicks(2981), "Eos qui molestiae non." },
                    { 14, new DateTime(2021, 7, 27, 22, 40, 51, 703, DateTimeKind.Local).AddTicks(2943), "Totam adipisci modi asperiores a." },
                    { 13, new DateTime(2021, 7, 27, 22, 40, 51, 703, DateTimeKind.Local).AddTicks(2895), "Expedita vel corporis est voluptatum commodi numquam saepe sint incidunt." },
                    { 12, new DateTime(2021, 7, 27, 22, 40, 51, 703, DateTimeKind.Local).AddTicks(2862), "Ullam ut ut iste." },
                    { 11, new DateTime(2021, 7, 27, 22, 40, 51, 703, DateTimeKind.Local).AddTicks(2810), "Aut recusandae aut." },
                    { 20, new DateTime(2021, 7, 27, 22, 40, 51, 703, DateTimeKind.Local).AddTicks(3166), "Illum impedit consequatur dolor sequi fugiat voluptas." },
                    { 9, new DateTime(2021, 7, 27, 22, 40, 51, 703, DateTimeKind.Local).AddTicks(2750), "Quos enim aut odio." },
                    { 8, new DateTime(2021, 7, 27, 22, 40, 51, 703, DateTimeKind.Local).AddTicks(2711), "Qui quibusdam voluptatem facere ut." },
                    { 7, new DateTime(2021, 7, 27, 22, 40, 51, 703, DateTimeKind.Local).AddTicks(2674), "Illo ea ut odio ut ipsum et." },
                    { 6, new DateTime(2021, 7, 27, 22, 40, 51, 703, DateTimeKind.Local).AddTicks(2643), "Hic in possimus saepe." },
                    { 5, new DateTime(2021, 7, 27, 22, 40, 51, 703, DateTimeKind.Local).AddTicks(2599), "Ratione facilis accusantium et modi ut ea atque." },
                    { 4, new DateTime(2021, 7, 27, 22, 40, 51, 703, DateTimeKind.Local).AddTicks(2556), "Impedit eius est dolorem qui sed." },
                    { 3, new DateTime(2021, 7, 27, 22, 40, 51, 703, DateTimeKind.Local).AddTicks(2481), "Consequatur magnam unde doloribus qui." },
                    { 2, new DateTime(2021, 7, 27, 22, 40, 51, 703, DateTimeKind.Local).AddTicks(2258), "Voluptatem eveniet soluta ratione qui dicta fuga minima unde laboriosam." },
                    { 10, new DateTime(2021, 7, 27, 22, 40, 51, 703, DateTimeKind.Local).AddTicks(2778), "Ut repellat quia inventore consequuntur." }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "BirthDay", "Email", "FirstName", "LastName", "RegisteredAt", "TeamId" },
                values: new object[,]
                {
                    { 12, new DateTime(2021, 7, 27, 22, 40, 51, 692, DateTimeKind.Local).AddTicks(8533), "Ottis.Williamson@hotmail.com", "Ava73", "Melissa_Keebler", new DateTime(2021, 7, 27, 22, 40, 51, 692, DateTimeKind.Local).AddTicks(8630), null },
                    { 13, new DateTime(2021, 7, 27, 22, 40, 51, 692, DateTimeKind.Local).AddTicks(8645), "Jedediah_Collins22@gmail.com", "Florine.Brakus1", "Malcolm40", new DateTime(2021, 7, 27, 22, 40, 51, 692, DateTimeKind.Local).AddTicks(8775), null },
                    { 14, new DateTime(2021, 7, 27, 22, 40, 51, 692, DateTimeKind.Local).AddTicks(8792), "Dax_Sauer65@gmail.com", "Kaylin.Powlowski", "Grayce94", new DateTime(2021, 7, 27, 22, 40, 51, 692, DateTimeKind.Local).AddTicks(8905), null },
                    { 18, new DateTime(2021, 7, 27, 22, 40, 51, 692, DateTimeKind.Local).AddTicks(9352), "Brent_McCullough88@gmail.com", "Dock_Purdy48", "Jovan_Daniel", new DateTime(2021, 7, 27, 22, 40, 51, 692, DateTimeKind.Local).AddTicks(9477), null },
                    { 16, new DateTime(2021, 7, 27, 22, 40, 51, 692, DateTimeKind.Local).AddTicks(9044), "Cade15@hotmail.com", "Ora_Schumm", "Laurence.Hamill42", new DateTime(2021, 7, 27, 22, 40, 51, 692, DateTimeKind.Local).AddTicks(9219), null },
                    { 17, new DateTime(2021, 7, 27, 22, 40, 51, 692, DateTimeKind.Local).AddTicks(9239), "Tyree.Davis45@hotmail.com", "Montana93", "Janick_Johnston", new DateTime(2021, 7, 27, 22, 40, 51, 692, DateTimeKind.Local).AddTicks(9335), null },
                    { 11, new DateTime(2021, 7, 27, 22, 40, 51, 692, DateTimeKind.Local).AddTicks(8392), "Jacinthe.Osinski68@gmail.com", "Carolyne46", "Myron88", new DateTime(2021, 7, 27, 22, 40, 51, 692, DateTimeKind.Local).AddTicks(8516), null },
                    { 15, new DateTime(2021, 7, 27, 22, 40, 51, 692, DateTimeKind.Local).AddTicks(8922), "Dudley_Runolfsdottir@yahoo.com", "Sabina.Gutmann", "Ofelia.Volkman42", new DateTime(2021, 7, 27, 22, 40, 51, 692, DateTimeKind.Local).AddTicks(9028), null },
                    { 10, new DateTime(2021, 7, 27, 22, 40, 51, 692, DateTimeKind.Local).AddTicks(8247), "Waylon44@yahoo.com", "Bennie3", "Berta.Leuschke88", new DateTime(2021, 7, 27, 22, 40, 51, 692, DateTimeKind.Local).AddTicks(8375), null },
                    { 6, new DateTime(2021, 7, 27, 22, 40, 51, 692, DateTimeKind.Local).AddTicks(7741), "Amie.Runte@yahoo.com", "Garrison.Beatty60", "Corbin_Swaniawski", new DateTime(2021, 7, 27, 22, 40, 51, 692, DateTimeKind.Local).AddTicks(7864), null },
                    { 8, new DateTime(2021, 7, 27, 22, 40, 51, 692, DateTimeKind.Local).AddTicks(7991), "Arnoldo8@gmail.com", "Veda17", "Destiney_Monahan65", new DateTime(2021, 7, 27, 22, 40, 51, 692, DateTimeKind.Local).AddTicks(8119), null },
                    { 7, new DateTime(2021, 7, 27, 22, 40, 51, 692, DateTimeKind.Local).AddTicks(7881), "Jared76@hotmail.com", "Martine87", "Stone.Leuschke94", new DateTime(2021, 7, 27, 22, 40, 51, 692, DateTimeKind.Local).AddTicks(7975), null },
                    { 5, new DateTime(2021, 7, 27, 22, 40, 51, 692, DateTimeKind.Local).AddTicks(7575), "Aracely_Considine@yahoo.com", "Kenyon3", "Corene_McCullough91", new DateTime(2021, 7, 27, 22, 40, 51, 692, DateTimeKind.Local).AddTicks(7722), null },
                    { 4, new DateTime(2021, 7, 27, 22, 40, 51, 692, DateTimeKind.Local).AddTicks(7446), "Lillie.Crona94@yahoo.com", "Domenico.Streich", "Emilio17", new DateTime(2021, 7, 27, 22, 40, 51, 692, DateTimeKind.Local).AddTicks(7558), null },
                    { 3, new DateTime(2021, 7, 27, 22, 40, 51, 692, DateTimeKind.Local).AddTicks(7193), "Daisy.Tromp96@gmail.com", "Sheridan71", "Harrison.Maggio38", new DateTime(2021, 7, 27, 22, 40, 51, 692, DateTimeKind.Local).AddTicks(7423), null },
                    { 2, new DateTime(2021, 7, 27, 22, 40, 51, 692, DateTimeKind.Local).AddTicks(6088), "Brett_Wiza@yahoo.com", "Armani87", "Nathen14", new DateTime(2021, 7, 27, 22, 40, 51, 692, DateTimeKind.Local).AddTicks(7143), null },
                    { 1, new DateTime(2021, 7, 27, 22, 40, 51, 680, DateTimeKind.Local).AddTicks(7176), "Sydnie97@yahoo.com", "Theo40", "Moises78", new DateTime(2021, 7, 27, 22, 40, 51, 692, DateTimeKind.Local).AddTicks(4780), null },
                    { 19, new DateTime(2021, 7, 27, 22, 40, 51, 692, DateTimeKind.Local).AddTicks(9495), "Eldridge92@yahoo.com", "Milford_Beahan", "Lionel.Hilpert", new DateTime(2021, 7, 27, 22, 40, 51, 692, DateTimeKind.Local).AddTicks(9617), null },
                    { 9, new DateTime(2021, 7, 27, 22, 40, 51, 692, DateTimeKind.Local).AddTicks(8136), "Vella10@gmail.com", "Camylle.Osinski99", "Joan.Schuppe19", new DateTime(2021, 7, 27, 22, 40, 51, 692, DateTimeKind.Local).AddTicks(8231), null },
                    { 20, new DateTime(2021, 7, 27, 22, 40, 51, 692, DateTimeKind.Local).AddTicks(9635), "Paolo.Mueller14@yahoo.com", "Rodrigo_OHara", "Scarlett_Kuhn89", new DateTime(2021, 7, 27, 22, 40, 51, 695, DateTimeKind.Local).AddTicks(8102), null }
                });

            migrationBuilder.InsertData(
                table: "Projects",
                columns: new[] { "Id", "AuthorId", "CreatedAt", "Deadline", "Description", "Name", "TeamId" },
                values: new object[,]
                {
                    { 5, 3, new DateTime(2021, 7, 27, 22, 40, 51, 710, DateTimeKind.Local).AddTicks(3620), new DateTime(2021, 7, 27, 22, 40, 51, 710, DateTimeKind.Local).AddTicks(3750), "Laborum consectetur tempore quia.\nSit autem praesentium modi rerum.\nLabore molestiae mollitia et doloremque accusantium iste.\nDeleniti quia aperiam.", "Veritatis delectus sed.", 16 },
                    { 17, 18, new DateTime(2021, 7, 27, 22, 40, 51, 710, DateTimeKind.Local).AddTicks(6992), new DateTime(2021, 7, 27, 22, 40, 51, 710, DateTimeKind.Local).AddTicks(7119), "Quae animi officiis.\nDolorem cum aut enim dolorum quis.\nSunt magnam et qui.\nDignissimos ut earum accusantium.\nVeritatis minus optio cupiditate aliquid.", "Placeat sit ipsam.", 20 },
                    { 9, 18, new DateTime(2021, 7, 27, 22, 40, 51, 710, DateTimeKind.Local).AddTicks(4361), new DateTime(2021, 7, 27, 22, 40, 51, 710, DateTimeKind.Local).AddTicks(5410), "Laborum aperiam qui ut accusantium laboriosam id inventore.\nAut et enim esse placeat.\nIllo impedit dignissimos ut quae culpa placeat impedit.\nBlanditiis fugit cupiditate voluptatem molestiae rerum illum commodi assumenda.\nMaiores molestiae et temporibus sit sit officiis aut ex.", "Sed cum voluptas consequatur aspernatur aut facilis rem est quod.", 4 },
                    { 14, 17, new DateTime(2021, 7, 27, 22, 40, 51, 710, DateTimeKind.Local).AddTicks(6221), new DateTime(2021, 7, 27, 22, 40, 51, 710, DateTimeKind.Local).AddTicks(6375), "Voluptatem voluptatum animi.\nMolestias ut necessitatibus nostrum odit adipisci quidem quae quam.\nAlias et eum perferendis ullam.\nEt tempore maiores dolor ut aliquid explicabo voluptas quis.", "Id qui velit saepe architecto ipsam ut quos iure.", 3 },
                    { 3, 15, new DateTime(2021, 7, 27, 22, 40, 51, 710, DateTimeKind.Local).AddTicks(3252), new DateTime(2021, 7, 27, 22, 40, 51, 710, DateTimeKind.Local).AddTicks(3467), "Placeat amet sit fuga aut ipsa.\nDolore et amet non enim id quos.\nPerferendis doloribus et quis illo eum dicta.\nVoluptatem et provident aut nemo accusamus iste beatae eos.", "Et sunt accusantium.", 2 },
                    { 6, 14, new DateTime(2021, 7, 27, 22, 40, 51, 710, DateTimeKind.Local).AddTicks(3764), new DateTime(2021, 7, 27, 22, 40, 51, 710, DateTimeKind.Local).AddTicks(3957), "Dolorum est deleniti et vel qui doloremque consequatur debitis.\nQui modi necessitatibus consequuntur similique.\nUt itaque accusamus.\nAsperiores perferendis animi ipsa.\nIn magni laborum.\nBeatae voluptatem expedita dolorum.", "Voluptas nulla omnis repellat dolor et.", 1 },
                    { 4, 14, new DateTime(2021, 7, 27, 22, 40, 51, 710, DateTimeKind.Local).AddTicks(3483), new DateTime(2021, 7, 27, 22, 40, 51, 710, DateTimeKind.Local).AddTicks(3607), "Consequatur eos eos ratione magni magnam voluptas id.\nArchitecto aspernatur vero consectetur laudantium.\nDistinctio molestiae aut.\nSunt nesciunt minima vel nobis aut architecto consectetur.", "In sed recusandae quaerat sit.", 12 },
                    { 10, 12, new DateTime(2021, 7, 27, 22, 40, 51, 710, DateTimeKind.Local).AddTicks(5442), new DateTime(2021, 7, 27, 22, 40, 51, 710, DateTimeKind.Local).AddTicks(5664), "Consequatur voluptates vel porro harum laboriosam maiores natus.\nId omnis omnis nihil et nihil aut.\nSimilique qui quo enim.", "Aliquid occaecati et quam ullam ut cum doloremque expedita.", 3 },
                    { 8, 12, new DateTime(2021, 7, 27, 22, 40, 51, 710, DateTimeKind.Local).AddTicks(4169), new DateTime(2021, 7, 27, 22, 40, 51, 710, DateTimeKind.Local).AddTicks(4347), "Omnis qui sed debitis dolorem delectus ut.\nDolores voluptas atque quaerat distinctio est reprehenderit molestias.\nEa exercitationem minima aut quos.\nQui tenetur iste unde animi.\nBeatae inventore ipsa ex laborum quasi rerum quibusdam praesentium rerum.", "Facere repellendus et ex.", 19 },
                    { 12, 11, new DateTime(2021, 7, 27, 22, 40, 51, 710, DateTimeKind.Local).AddTicks(5908), new DateTime(2021, 7, 27, 22, 40, 51, 710, DateTimeKind.Local).AddTicks(6103), "Dolorem reiciendis quis aspernatur impedit.\nEst ipsum voluptatem repellendus.\nConsequuntur omnis qui sunt voluptates architecto molestiae consequatur.\nNon vel laudantium.\nConsequatur consequatur officiis maiores dolor.\nAsperiores omnis voluptas expedita voluptatem in sit velit saepe.", "Nam velit soluta autem culpa placeat voluptates cum error beatae.", 11 },
                    { 11, 11, new DateTime(2021, 7, 27, 22, 40, 51, 710, DateTimeKind.Local).AddTicks(5681), new DateTime(2021, 7, 27, 22, 40, 51, 710, DateTimeKind.Local).AddTicks(5893), "Odit quia sequi vel sit aut reiciendis quia nesciunt dolore.\nQui est quae aut similique.\nTotam laborum consequatur doloremque natus exercitationem quibusdam.\nEaque quidem aut totam.\nRerum neque iusto earum voluptatem cum perferendis consectetur vel nihil.\nId consectetur ad qui fugiat.", "Corporis officiis laudantium quasi et maiores dolorum.", 19 },
                    { 2, 10, new DateTime(2021, 7, 27, 22, 40, 51, 710, DateTimeKind.Local).AddTicks(2744), new DateTime(2021, 7, 27, 22, 40, 51, 710, DateTimeKind.Local).AddTicks(3224), "Ducimus laudantium reiciendis nesciunt a facilis unde et.\nHarum quo non velit ea cumque possimus.", "Aut aut sit nisi cupiditate non explicabo pariatur sunt dicta.", 10 },
                    { 20, 8, new DateTime(2021, 7, 27, 22, 40, 51, 710, DateTimeKind.Local).AddTicks(7411), new DateTime(2021, 7, 27, 22, 40, 51, 710, DateTimeKind.Local).AddTicks(7555), "Quaerat praesentium dolorem nihil repellendus quidem.\nLaboriosam ducimus ut deserunt sequi.\nId commodi ut voluptas ratione quos facere itaque nobis.\nA molestias pariatur veritatis itaque.\nEst aut et et.", "Reiciendis quod voluptatem.", 8 },
                    { 7, 7, new DateTime(2021, 7, 27, 22, 40, 51, 710, DateTimeKind.Local).AddTicks(3973), new DateTime(2021, 7, 27, 22, 40, 51, 710, DateTimeKind.Local).AddTicks(4154), "Eveniet hic ratione recusandae quos nulla incidunt illum enim.\nVoluptas est quia odio vel ut eligendi.\nConsequuntur et alias non id quas.\nEst ea aut nesciunt fuga modi suscipit suscipit est.\nAut et velit sed.", "Nulla dignissimos eum consequatur perspiciatis quisquam.", 14 },
                    { 15, 5, new DateTime(2021, 7, 27, 22, 40, 51, 710, DateTimeKind.Local).AddTicks(6390), new DateTime(2021, 7, 27, 22, 40, 51, 710, DateTimeKind.Local).AddTicks(6556), "Laboriosam possimus nemo vel aliquid tempore quia debitis vitae.\nUnde mollitia deserunt corrupti illum sit nesciunt qui quae consequatur.\nDeserunt dolores nulla non ad.", "Veniam saepe fuga nesciunt architecto eius et est.", 1 },
                    { 19, 4, new DateTime(2021, 7, 27, 22, 40, 51, 710, DateTimeKind.Local).AddTicks(7224), new DateTime(2021, 7, 27, 22, 40, 51, 710, DateTimeKind.Local).AddTicks(7397), "Voluptatem aliquam corporis nostrum aliquam vitae culpa est.\nImpedit itaque voluptatem.\nEnim voluptas natus ex rerum exercitationem harum voluptatem.\nAsperiores nulla laudantium fugiat exercitationem rerum ea est voluptatem.\nMolestiae error quos labore velit sunt et.\nEos ut necessitatibus enim.", "Molestias voluptas dignissimos rerum nihil excepturi ut.", 13 },
                    { 13, 4, new DateTime(2021, 7, 27, 22, 40, 51, 710, DateTimeKind.Local).AddTicks(6119), new DateTime(2021, 7, 27, 22, 40, 51, 710, DateTimeKind.Local).AddTicks(6208), "Ratione temporibus excepturi eaque sunt cumque laboriosam dolorum asperiores suscipit.\nHic commodi eveniet autem est in officiis provident reprehenderit.", "Quibusdam optio tempore facere quaerat.", 2 },
                    { 1, 4, new DateTime(2021, 7, 27, 22, 40, 51, 708, DateTimeKind.Local).AddTicks(2181), new DateTime(2021, 7, 27, 22, 40, 51, 710, DateTimeKind.Local).AddTicks(1770), "Omnis rem vitae nisi eos consequuntur.\nOmnis corrupti ea et ut omnis cumque.\nDoloribus et ipsum qui ipsum.\nDolorum sint iusto autem occaecati non.\nItaque ad asperiores architecto similique sit maxime quia rerum id.", "Officiis labore voluptas assumenda nihil aliquid voluptatem ipsa aut.", 10 },
                    { 18, 19, new DateTime(2021, 7, 27, 22, 40, 51, 710, DateTimeKind.Local).AddTicks(7134), new DateTime(2021, 7, 27, 22, 40, 51, 710, DateTimeKind.Local).AddTicks(7213), "Nisi iste dignissimos quas dolore tempora facere quos.\nVel illo incidunt quisquam doloribus sint voluptatem.", "Non ipsa porro est maiores accusamus.", 4 },
                    { 16, 20, new DateTime(2021, 7, 27, 22, 40, 51, 710, DateTimeKind.Local).AddTicks(6572), new DateTime(2021, 7, 27, 22, 40, 51, 710, DateTimeKind.Local).AddTicks(6973), "Ipsum neque tenetur occaecati et et exercitationem deleniti.\nProvident neque incidunt alias facere minima quos maxime voluptatem.\nPariatur beatae maiores voluptas voluptatibus praesentium.\nVoluptatum vel nobis voluptas temporibus aut sit in.\nNulla quia quasi magnam adipisci incidunt autem vero placeat minus.\nNesciunt voluptatem itaque esse ut voluptas.", "Quia nam sit voluptatem ut inventore quas quia repudiandae.", 16 }
                });

            migrationBuilder.InsertData(
                table: "Tasks",
                columns: new[] { "Id", "CreatedAt", "Description", "FinishedAt", "Name", "PerformerId", "ProjectId", "State" },
                values: new object[,]
                {
                    { 2, new DateTime(2021, 7, 27, 22, 40, 51, 716, DateTimeKind.Local).AddTicks(1676), "Iure qui quia ipsa suscipit sit dolor debitis molestiae.\nEt omnis doloremque.\nIn deserunt qui praesentium nihil non voluptatum.\nQui illum similique itaque rerum expedita.\nDolore ut eligendi cumque eaque aut.\nAccusantium dolorem culpa facere autem omnis et optio officiis.", new DateTime(2021, 7, 27, 22, 40, 51, 716, DateTimeKind.Local).AddTicks(1979), "Quis ut ut quisquam fuga dolorum alias laudantium.", 16, 5, 4 },
                    { 17, new DateTime(2021, 7, 27, 22, 40, 51, 716, DateTimeKind.Local).AddTicks(4126), "Consequatur recusandae et et facilis.\nQui omnis blanditiis quo officiis in officia corrupti id.\nLaudantium dicta assumenda fugit sit quidem fuga.\nEsse dicta in in id esse minima iste qui doloremque.\nEos praesentium id sit harum qui accusantium et illum.\nQuia placeat aliquam.", new DateTime(2021, 7, 27, 22, 40, 51, 716, DateTimeKind.Local).AddTicks(4298), "Quod id commodi possimus nam.", 1, 17, 2 },
                    { 5, new DateTime(2021, 7, 27, 22, 40, 51, 716, DateTimeKind.Local).AddTicks(2426), "Nulla eius eligendi neque totam vel culpa.\nAut qui non beatae at rerum id.\nEst impedit modi.", new DateTime(2021, 7, 27, 22, 40, 51, 716, DateTimeKind.Local).AddTicks(2520), "Error voluptatum non.", 5, 17, 2 },
                    { 3, new DateTime(2021, 7, 27, 22, 40, 51, 716, DateTimeKind.Local).AddTicks(2023), "Consequatur cum nemo voluptatum qui.\nQuia iusto soluta autem.", new DateTime(2021, 7, 27, 22, 40, 51, 716, DateTimeKind.Local).AddTicks(2095), "Modi doloremque id consequatur.", 17, 17, 2 },
                    { 11, new DateTime(2021, 7, 27, 22, 40, 51, 716, DateTimeKind.Local).AddTicks(3378), "Sed omnis nostrum quia et omnis nihil expedita at voluptates.\nVoluptatem alias at in.\nFacere cum odit est.\nTempore quisquam asperiores sit et aut est.\nQuod incidunt non consequuntur facere.", new DateTime(2021, 7, 27, 22, 40, 51, 716, DateTimeKind.Local).AddTicks(3528), "Similique ut possimus ratione laborum iusto eos.", 15, 9, 1 },
                    { 9, new DateTime(2021, 7, 27, 22, 40, 51, 716, DateTimeKind.Local).AddTicks(3041), "Quos asperiores dolorum consectetur ut ratione.\nA quidem amet laudantium unde sunt velit consequatur.\nEum quam officiis recusandae at debitis ea numquam laudantium excepturi.\nAtque et in consequatur autem.", new DateTime(2021, 7, 27, 22, 40, 51, 716, DateTimeKind.Local).AddTicks(3160), "Voluptatibus minima aut.", 14, 3, 3 },
                    { 6, new DateTime(2021, 7, 27, 22, 40, 51, 716, DateTimeKind.Local).AddTicks(2564), "Id rerum ex perferendis et vel iusto.\nLaudantium harum veniam est atque rerum dolores.\nQuo vitae et totam eum et.", new DateTime(2021, 7, 27, 22, 40, 51, 716, DateTimeKind.Local).AddTicks(2673), "Facilis cupiditate illo reiciendis dicta.", 2, 3, 3 },
                    { 13, new DateTime(2021, 7, 27, 22, 40, 51, 716, DateTimeKind.Local).AddTicks(3641), "Non culpa deserunt non rem at dolorum dolor culpa rem.\nQuasi eos aut inventore quae.", new DateTime(2021, 7, 27, 22, 40, 51, 716, DateTimeKind.Local).AddTicks(3733), "Soluta enim saepe.", 19, 4, 1 },
                    { 1, new DateTime(2021, 7, 27, 22, 40, 51, 715, DateTimeKind.Local).AddTicks(1195), "Optio rem eligendi necessitatibus.\nIn ratione et sint itaque sunt.", new DateTime(2021, 7, 27, 22, 40, 51, 715, DateTimeKind.Local).AddTicks(7754), "Et omnis et amet sint ipsum nam.", 9, 4, 2 },
                    { 18, new DateTime(2021, 7, 27, 22, 40, 51, 716, DateTimeKind.Local).AddTicks(4332), "Iste autem illum ipsa.\nIste ipsam quaerat.\nDolorum debitis illo vero et dolores nam.\nQuia aut et quia laboriosam.\nIn necessitatibus quia cumque.", new DateTime(2021, 7, 27, 22, 40, 51, 716, DateTimeKind.Local).AddTicks(4456), "In minus voluptas repellat consequatur.", 17, 8, 2 },
                    { 12, new DateTime(2021, 7, 27, 22, 40, 51, 716, DateTimeKind.Local).AddTicks(3550), "Esse unde ut ut placeat laudantium soluta sit voluptatem delectus.\nDignissimos eum exercitationem aut vel temporibus.", new DateTime(2021, 7, 27, 22, 40, 51, 716, DateTimeKind.Local).AddTicks(3622), "Quia vitae culpa magnam.", 2, 8, 3 },
                    { 8, new DateTime(2021, 7, 27, 22, 40, 51, 716, DateTimeKind.Local).AddTicks(2830), "Deleniti consectetur et quos.\nAlias facere rerum aut nihil aliquam molestias voluptatem.\nEt vel omnis molestiae aliquid porro sapiente.\nProvident nulla at rem sed.\nOmnis reprehenderit cumque ad.\nQui soluta nobis a eveniet voluptatem dolores aliquam.", new DateTime(2021, 7, 27, 22, 40, 51, 716, DateTimeKind.Local).AddTicks(3017), "Ullam consequuntur labore ut quam.", 10, 2, 4 },
                    { 19, new DateTime(2021, 7, 27, 22, 40, 51, 716, DateTimeKind.Local).AddTicks(4477), "Quidem maiores reprehenderit voluptatem optio.\nConsectetur quidem quia autem iusto excepturi mollitia aut optio neque.\nPossimus consequatur eius incidunt et voluptatem et.\nQuos rerum non quia quia ad totam id debitis.", new DateTime(2021, 7, 27, 22, 40, 51, 716, DateTimeKind.Local).AddTicks(4583), "Et omnis dolor.", 1, 7, 2 },
                    { 15, new DateTime(2021, 7, 27, 22, 40, 51, 716, DateTimeKind.Local).AddTicks(3835), "Qui officiis eum ea consequatur.\nNecessitatibus quod voluptatem natus nobis occaecati iusto.\nDoloremque nostrum aspernatur ut tenetur praesentium esse tempore pariatur.\nProvident non iusto et architecto repudiandae.\nAut et dolor non nihil hic ex provident.", new DateTime(2021, 7, 27, 22, 40, 51, 716, DateTimeKind.Local).AddTicks(3981), "Porro labore qui.", 12, 15, 1 },
                    { 20, new DateTime(2021, 7, 27, 22, 40, 51, 716, DateTimeKind.Local).AddTicks(4605), "Suscipit quae voluptatem eum et.\nReprehenderit quo rem.\nSed et distinctio excepturi et occaecati.", new DateTime(2021, 7, 27, 22, 40, 51, 716, DateTimeKind.Local).AddTicks(4725), "Ut tempora quaerat voluptatem molestias qui aspernatur nobis consectetur.", 20, 19, 1 },
                    { 16, new DateTime(2021, 7, 27, 22, 40, 51, 716, DateTimeKind.Local).AddTicks(4007), "Rerum illo voluptatem commodi.\nAutem architecto ut nam ea pariatur aut necessitatibus expedita.", new DateTime(2021, 7, 27, 22, 40, 51, 716, DateTimeKind.Local).AddTicks(4106), "Ipsum delectus eos non minima qui voluptatem molestiae eos.", 18, 13, 2 },
                    { 14, new DateTime(2021, 7, 27, 22, 40, 51, 716, DateTimeKind.Local).AddTicks(3753), "Itaque voluptatem qui expedita exercitationem.\nOmnis sed aut officia quos quos cupiditate qui ab.", new DateTime(2021, 7, 27, 22, 40, 51, 716, DateTimeKind.Local).AddTicks(3817), "Nostrum qui molestiae quasi nihil.", 15, 13, 4 },
                    { 4, new DateTime(2021, 7, 27, 22, 40, 51, 716, DateTimeKind.Local).AddTicks(2117), "Repellendus et voluptate rerum tempora sit quidem libero.\nBeatae quisquam aliquid.\nExcepturi libero fugit error.\nVeritatis hic non nihil culpa laudantium aperiam magni nobis non.\nEos incidunt commodi nihil distinctio blanditiis.", new DateTime(2021, 7, 27, 22, 40, 51, 716, DateTimeKind.Local).AddTicks(2385), "Qui facilis similique sunt debitis tempore dolores molestiae qui ducimus.", 14, 1, 4 },
                    { 10, new DateTime(2021, 7, 27, 22, 40, 51, 716, DateTimeKind.Local).AddTicks(3178), "Quae alias ducimus soluta officiis in sunt laborum.\nVoluptate qui possimus debitis quia sunt inventore.\nTempore at delectus molestias exercitationem voluptas laboriosam a laboriosam.", new DateTime(2021, 7, 27, 22, 40, 51, 716, DateTimeKind.Local).AddTicks(3350), "Impedit et corrupti laudantium sed accusantium alias voluptatem quo tempore.", 19, 18, 3 },
                    { 7, new DateTime(2021, 7, 27, 22, 40, 51, 716, DateTimeKind.Local).AddTicks(2693), "Quisquam quo sit autem distinctio quia dolore officia.\nNeque officiis omnis reprehenderit cum eos consequatur sunt accusamus possimus.", new DateTime(2021, 7, 27, 22, 40, 51, 716, DateTimeKind.Local).AddTicks(2806), "Quos sunt dolorem odit corrupti est illo quia.", 5, 16, 2 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Projects_AuthorId",
                table: "Projects",
                column: "AuthorId");

            migrationBuilder.CreateIndex(
                name: "IX_Projects_TeamId",
                table: "Projects",
                column: "TeamId",
                unique: true,
                filter: "[TeamId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Tasks_PerformerId",
                table: "Tasks",
                column: "PerformerId");

            migrationBuilder.CreateIndex(
                name: "IX_Tasks_ProjectId",
                table: "Tasks",
                column: "ProjectId");

            migrationBuilder.CreateIndex(
                name: "IX_Users_TeamId",
                table: "Users",
                column: "TeamId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Tasks");

            migrationBuilder.DropTable(
                name: "Projects");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropTable(
                name: "Teams");
        }
    }
}
