﻿using PS.Domain.DAL.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace PS.Domain.Entities
{
    public class Project : BaseEntity
    {
        public Project()
        {
            Tasks = new Collection<Task>();
        }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime Deadline { get; set; }

        public int? TeamId { get; set; }
        public Team Team { get; set; }

        public int AuthorId { get; set; }
        public User User { get; set; }

        public ICollection<Task> Tasks { get; set; }
    }
}
