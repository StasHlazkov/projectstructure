﻿using PS.Domain.DAL.Entities.Abstract;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace PS.Domain.Entities
{
    public class Team : BaseEntity
    {
        public Team()
        {
            Users = new Collection<User>();
        }
        public string Name { get; set; }
        public Project Project { get; set; }
        public ICollection<User> Users { get; set; }
    }
}
