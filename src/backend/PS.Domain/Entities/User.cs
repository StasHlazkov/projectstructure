﻿using PS.Domain.DAL.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace PS.Domain.Entities
{
    public class User : BaseEntity
    {
        public User()
        {
            Tasks = new Collection<Task>();
        }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime RegisteredAt { get; set; }
        public DateTime BirthDay { get; set; }

        public int? TeamId { get; set; }
        public Team Team { get; set; }

        public ICollection<Project> Projects { get; set; }

        public ICollection<Task> Tasks { get; set; }

    }
}
