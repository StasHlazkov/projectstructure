﻿using Microsoft.EntityFrameworkCore;
using PS.Domain.Entities;

namespace PS.Domain.DAL.Context
{
    public class ApplicationContext : DbContext
    {
        public DbSet<User> Users { get; private set; }
        public DbSet<Team> Teams { get; private set; }
        public DbSet<Task> Tasks { get; private set; }
        public DbSet<Project> Projects { get; private set; }

        public ApplicationContext(DbContextOptions<ApplicationContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Setting up entities using extension method
            modelBuilder.Configure();

            // Seeding data using extension method
            // NOTE: this method will be called every time after adding a new migration, cuz we use Bogus for seed data
            modelBuilder.Seed();
        }
    }
}
