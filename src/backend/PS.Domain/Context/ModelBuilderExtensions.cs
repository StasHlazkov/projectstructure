﻿using Bogus;
using Microsoft.EntityFrameworkCore;
using PS.Domain.Entities;
using System;
using System.Collections.Generic;

namespace PS.Domain.DAL.Context
{
    public static class ModelBuilderExtensions
    {
        private const int ENTITY_COUNT = 20;
        public static void Configure(this ModelBuilder modelBuilder)
        {
            modelBuilder
                .Entity<User>()
                .Ignore(u => u.CreatedAt)
                .HasMany(u => u.Tasks)
                .WithOne(t => t.User)
                .HasForeignKey(t => t.PerformerId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder
                .Entity<Team>()
                .HasOne(t => t.Project)
                .WithOne(p => p.Team)
                .HasForeignKey<Project>(p => p.TeamId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder
                .Entity<Team>()
                .HasMany(t => t.Users)
                .WithOne(u => u.Team)
                .HasForeignKey(t => t.TeamId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder
                .Entity<Project>()
                .HasMany(p => p.Tasks)
                .WithOne(t => t.Project)
                .HasForeignKey(p => p.ProjectId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<User>()
                .HasMany(u => u.Projects)
                .WithOne(p => p.User)
                .HasForeignKey(u => u.AuthorId)
                .OnDelete(DeleteBehavior.Restrict);
        }

        public static void Seed(this ModelBuilder modelBuilder)
        {
            var users = GenerateRandomUsers();
            var teams = GenerateRandomTeams();
            var projects = GenerateRandomProjects(users, teams);
            var tasks = GenerateRandomTasks(projects, users);

            modelBuilder.Entity<User>().HasData(users);
            modelBuilder.Entity<Team>().HasData(teams);
            modelBuilder.Entity<Project>().HasData(projects);
            modelBuilder.Entity<Task>().HasData(tasks);
        }

        public static ICollection<User> GenerateRandomUsers()
        {
            int UserId = 1;
            var UserFacke = new Faker<User>()
                .RuleFor(u => u.Id , f => UserId++)
                .RuleFor(u => u.CreatedAt, f => DateTime.Now)
                .RuleFor(u => u.BirthDay, f => DateTime.Now)
                .RuleFor(u => u.FirstName, f => f.Internet.UserName())
                .RuleFor(u => u.LastName, f => f.Internet.UserName())
                .RuleFor(u => u.Email, f => f.Internet.Email())
                .RuleFor(u => u.RegisteredAt, f => DateTime.Now);

            var generateUser = UserFacke.Generate(ENTITY_COUNT);

            return generateUser;
        }

        public static ICollection<Project> GenerateRandomProjects(ICollection<User> users, ICollection<Team> teams)
        {
            int ProjectId = 1;

            var ProjectsFacke = new Faker<Project>()
                .RuleFor(u => u.Id, (f => ProjectId++))
                .RuleFor(u => u.CreatedAt, f => DateTime.Now)
                .RuleFor(u => u.Description, f => f.Lorem.Sentences())
                .RuleFor(u => u.Name, f => f.Lorem.Sentence())
                .RuleFor(u => u.TeamId, f => f.PickRandom(teams).Id)
                .RuleFor(u => u.AuthorId, f => f.PickRandom(users).Id)
                .RuleFor(u => u.Deadline, f => DateTime.Now);

            var generateUser = ProjectsFacke.Generate(ENTITY_COUNT);

            return generateUser;
        }

        public static ICollection<Team> GenerateRandomTeams()
        {
            int TeamId = 1;

            var TeamsFacke = new Faker<Team>()
                .RuleFor(u => u.Id, (f => TeamId++))
                .RuleFor(u => u.CreatedAt, f => DateTime.Now)
                .RuleFor(u => u.Name, f => f.Lorem.Sentence());


            var generateTeams = TeamsFacke.Generate(ENTITY_COUNT);

            return generateTeams;
        }

        public static ICollection<Task> GenerateRandomTasks(ICollection<Project> projects, ICollection<User> users)
        {
            int[] state = new int[] { 1, 2, 3, 4 };
            int TeamId = 1;

            var TeamsFacke = new Faker<Task>()
                .RuleFor(u => u.Id, (f => TeamId++))
                .RuleFor(u => u.CreatedAt, f => DateTime.Now)
                .RuleFor(u => u.Name, f => f.Lorem.Sentence())
                .RuleFor(u => u.State, f => f.PickRandom(state))
                .RuleFor(u => u.Description, f => f.Lorem.Sentences())
                .RuleFor(u => u.FinishedAt, f => DateTime.Now)
                .RuleFor(u => u.PerformerId, f => f.PickRandom(users).Id)
                .RuleFor(u => u.ProjectId, f => f.PickRandom(projects).Id);


            var generateTeams = TeamsFacke.Generate(ENTITY_COUNT);

            return generateTeams;
        }
    }
}
