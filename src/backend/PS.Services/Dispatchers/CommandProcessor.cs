﻿using PS.Services.Abstractions;
using System;
using System.Threading.Tasks;

namespace PS.Services.Implementation
{
    public class CommandProcessor : ICommandProcessor
    {
        private readonly IServiceProvider _serviceProvider;
        public CommandProcessor(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }
        public async Task ProcessAsync<T>(T command) where T : ICommand
        {
            var service = _serviceProvider.GetService(typeof(IRequestHandler<T>)) as IRequestHandler<T>;
            await service.HandleAsync(command);
        }
    }
}
