﻿using Microsoft.EntityFrameworkCore;
using PS.Domain.DAL.Context;
using PS.Interfaces.IRepositories;
using PS.Services.GenericPepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EntityTask = PS.Domain.Entities;

namespace PS.Services.Repositories
{
    public class TeamRepository : GenericRepository<EntityTask.Team>, ITeamRepository
    {
        public TeamRepository(ApplicationContext context) : base(context) { }
        public async Task Create(EntityTask.Team item)
        {
            var team = await _context.Teams.Where(i => i.Id == item.Id).FirstOrDefaultAsync();
            if (team == null)
            {
                await _context.Teams.AddAsync(item);
            }
        }

        public async Task<EntityTask.Team> FindById(int id)
        {
            return await _context.Teams.Where(i => i.Id == id).FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<EntityTask.Team>> Get()
        {
            return await _context.Teams.ToListAsync();
        }

        public async Task<IEnumerable<EntityTask.Team>> Get(Func<EntityTask.Team, bool> predicate)
        {
            throw new NotImplementedException();
        }

        public async Task Remove(EntityTask.Team item)
        {
            var team = await _context.Teams.Where(i => i.Id == item.Id).FirstOrDefaultAsync();
            if (team != null)
            {
                 _context.Teams.Remove(item);
            }
        }

        public async Task Update(EntityTask.Team item)
        {
            var team = await _context.Teams.Where(i => i.Id == item.Id).FirstOrDefaultAsync();
            if (team != null)
            {
                team = item;
            }
        }
    }
}
