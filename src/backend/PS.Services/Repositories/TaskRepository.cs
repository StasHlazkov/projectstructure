﻿using Microsoft.EntityFrameworkCore;
using PS.Domain.DAL.Context;
using PS.Interfaces.IRepositories;
using PS.Services.GenericPepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EntityTask = PS.Domain.Entities;

namespace PS.Services.Repositories
{
    public class TaskRepository : GenericRepository<EntityTask.Task>, ITaskRepository
    {
        public TaskRepository(ApplicationContext context) : base(context) { }
        public async Task Create(EntityTask.Task item)
        {
            var task = await _context.Tasks.Where(i => i.Id == item.Id).FirstOrDefaultAsync();
            if (task == null)
            {
                await _context.Tasks.AddAsync(item);
            }
        }

        public async Task<EntityTask.Task> FindById(int id)
        {
            return await _context.Tasks.Where(i => i.Id == id).FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<EntityTask.Task>> Get()
        {
            return await _context.Tasks.ToListAsync();
        }

        public async Task<IEnumerable<EntityTask.Task>> Get(Func<EntityTask.Project, bool> predicate)
        {
            throw new NotImplementedException();
        }

        public async Task Remove(EntityTask.Task item)
        {
            var task = await _context.Tasks.Where(i => i.Id == item.Id).FirstOrDefaultAsync();
            if (task != null)
            {
                 _context.Tasks.Remove(item);
            }
        }

        public async Task Update(EntityTask.Task item)
        {
            var task = await _context.Tasks.Where(i => i.Id == item.Id).FirstOrDefaultAsync();
            if (task != null)
            {
                task = item;
            }
        }
    }
}
