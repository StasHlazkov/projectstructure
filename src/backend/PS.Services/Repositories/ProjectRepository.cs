﻿using Microsoft.EntityFrameworkCore;
using PS.Domain.DAL.Context;
using PS.Interfaces.IRepositories;
using PS.Services.GenericPepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EntityTask = PS.Domain.Entities;

namespace PS.Services.Repositories
{
    public class ProjectRepository : GenericRepository<EntityTask.Project>, IProjectRepository
    {
        public ProjectRepository(ApplicationContext context) : base(context)
        {
        }

        public async Task Create(EntityTask.Project item)
        {
           await _context.Projects.AddAsync(item);
        }

        public async Task<EntityTask.Project> FindById(int id)
        {
            return await _context.Projects.Where(i => i.Id == id).FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<EntityTask.Project>> Get()
        {
            return await _context.Projects.ToListAsync();
        }

        public async Task<IEnumerable<EntityTask.Project>> Get(Func<EntityTask.Project, bool> predicate)
        {
            throw new NotImplementedException();
        }

        public async Task Remove(EntityTask.Project item)
        {
            var project = await _context.Projects.Where(i => i.Id == item.Id).FirstOrDefaultAsync();
            if(project != null)
            {
                _context.Projects.Remove(item);
            }
        }

        public async Task Update(EntityTask.Project item)
        {
            var project = await _context.Projects.Where(i => i.Id == item.Id).FirstOrDefaultAsync();
            if (project != null)
            {
                project = item;
            }
        }
    }
}
