﻿using Microsoft.EntityFrameworkCore;
using PS.Domain.DAL.Context;
using PS.Interfaces.IRepositories;
using PS.Services.GenericPepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EntityTask = PS.Domain.Entities;

namespace PS.Services.Repositories
{
    public class UserRepository : GenericRepository<EntityTask.User>, IUserRepository
    {
        public UserRepository(ApplicationContext context) : base(context) { }
        public async Task Create(EntityTask.User item)
        {
            var user = await _context.Users.Where(i => i.Id == item.Id).FirstOrDefaultAsync();
            if (user == null)
            {
                await _context.Users.AddAsync(item);
            }
        }

        public async Task<EntityTask.User> FindById(int id)
        {
            return await _context.Users.Where(i => i.Id == id).FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<EntityTask.User>> Get()
        {
            return await _context.Users.ToListAsync();
        }

        public async Task<IEnumerable<EntityTask.User>> Get(Func<EntityTask.User, bool> predicate)
        {
            throw new NotImplementedException();
        }

        public async Task Remove(EntityTask.User item)
        {
            var user = await _context.Users.Where(i => i.Id == item.Id).FirstOrDefaultAsync();
            if (user != null)
            {
                 _context.Users.Remove(item);
            }
        }

        public async Task Update(EntityTask.User item)
        {
            var user = await _context.Users.Where(i => i.Id == item.Id).FirstOrDefaultAsync();
            if (user != null)
            {
                user = item;
            }
        }
    }
}
