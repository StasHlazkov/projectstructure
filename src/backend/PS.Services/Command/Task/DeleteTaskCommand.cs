﻿using MediatR;
using Newtonsoft.Json;

namespace PS.Services.Command.Task
{
    public class DeleteTaskCommand : IRequest
    {
        [JsonProperty("id")]
        public int Id { get; set; }
    }
}
