﻿using MediatR;
using Newtonsoft.Json;
using System;

namespace PS.Services.Command.Task
{
    public class UpdateTaskCommand : IRequest
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        
        [JsonProperty("project_id")]
        public int ProjectId { get; set; }
       
        [JsonProperty("performe_id")]
        public int PerformerId { get; set; }
        
        [JsonProperty("name")]
        public string Name { get; set; }
        
        [JsonProperty("description")]
        public string Description { get; set; }
       
        [JsonProperty("state")]
        public int State { get; set; }
        
        [JsonProperty("created_at")]
        public DateTime CreatedAt { get; set; }
        
        [JsonProperty("finished_at")]
        public DateTime FinishedAt { get; set; }
    }
}
