﻿using MediatR;
using Newtonsoft.Json;
using System;

namespace PS.Services.Command.Task
{
    public class CreateTaskCommand : IRequest
    {
        [JsonProperty("project_id")]
        public int ProjectId { get; set; }

        [JsonProperty("performer_id")]
        public int? PerformerId { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("state")]
        public int State { get; set; }
    }
}
