﻿using MediatR;
using Newtonsoft.Json;

namespace PS.Services.Command.User
{
    public class DeleteUserCommand : IRequest
    {
        [JsonProperty("id")]
        public int Id { get; set; }
    }
}
