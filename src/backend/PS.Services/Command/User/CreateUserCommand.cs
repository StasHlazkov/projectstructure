﻿using MediatR;
using Newtonsoft.Json;
using System;

namespace PS.Services.Command.User
{
    public class CreateUserCommand : IRequest
    {
        [JsonProperty("team_id")]
        public int? TeamId { get; set; }

        [JsonProperty("first_name")]
        public string FirstName { get; set; }

        [JsonProperty("last_name")]
        public string LastName { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("registered_at")]
        public DateTime RegisteredAt { get; set; }

        [JsonProperty("birthday")]
        public DateTime BirthDay { get; set; }
    }
}
