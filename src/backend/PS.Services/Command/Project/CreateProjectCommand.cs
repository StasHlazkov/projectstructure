﻿using MediatR;
using Newtonsoft.Json;
using System;

namespace PS.Services.Command.Project
{
    public class CreateProjectCommand : IRequest
    {
        [JsonProperty("authorId")]
        public int AuthorId { get; set; }

        [JsonProperty("teamId")]
        public int? TeamId { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("deadline")]
        public DateTime Deadline { get; set; }
    }
}
