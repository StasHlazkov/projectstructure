﻿using MediatR;
using Newtonsoft.Json;
using System;

namespace PS.Services.Command.Project
{
    public class UpdateProjectCommand : IRequest
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("author_id")]
        public int AuthorId { get; set; }

        [JsonProperty("team_id")]
        public int TeamId { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("deadline")]
        public DateTime Deadline { get; set; }
    }
}
