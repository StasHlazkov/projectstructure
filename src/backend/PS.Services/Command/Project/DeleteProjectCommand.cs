﻿using MediatR;
using Newtonsoft.Json;

namespace PS.Services.Command.Project
{
    public class DeleteProjectCommand : IRequest
    {
        [JsonProperty("id")]
        public int Id { get; set; }
    }
}
