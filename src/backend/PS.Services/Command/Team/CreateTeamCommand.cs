﻿using MediatR;
using Newtonsoft.Json;

namespace PS.Services.Command.Team
{
    public class CreateTeamCommand : IRequest
    {
        [JsonProperty("name")]
        public string Name { get; set; }
    }
}
