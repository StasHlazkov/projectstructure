﻿using MediatR;
using Newtonsoft.Json;

namespace PS.Services.Command.Team
{
    public class UpdateTeamCommand : IRequest
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        
        [JsonProperty("name")]
        public string Name { get; set; }
    }
}
