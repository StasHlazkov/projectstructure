﻿using MediatR;
using Newtonsoft.Json;

namespace PS.Services.Command.Team
{
    public class DeleteTeamCommand : IRequest
    {
        [JsonProperty("id")]
        public int Id { get; set; }
    }
}
