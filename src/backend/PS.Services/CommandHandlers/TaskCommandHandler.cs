﻿using AutoMapper;
using MediatR;
using PS.Interfaces;
using PS.Services.Command.Task;
using System;
using System.Threading;
using System.Threading.Tasks;
using EntityTask = PS.Domain.Entities;

namespace PS.Services.CommandHandlers
{
    public class TaskCommandHandler : IRequestHandler<CreateTaskCommand>, IRequestHandler<UpdateTaskCommand>, IRequestHandler<DeleteTaskCommand>
    {
        private readonly IMapper mapper;
        private readonly IUnitOfWork unitOfWork;
        public TaskCommandHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            this.mapper = mapper;
            this.unitOfWork = unitOfWork;
        }

        public async Task<Unit> Handle(UpdateTaskCommand request, CancellationToken cancellationToken)
        {
            var task = await unitOfWork.Tasks.FindById(request.Id);
            if (task == null)
            {
                throw new Exception();
            }

            task.Name = request.Name;
            task.Description = request.Description;
            task.PerformerId = request.PerformerId;
            task.ProjectId = request.ProjectId;
            task.State = request.State;
            task.FinishedAt = request.FinishedAt;

            await unitOfWork.Tasks.Update(task);

            return Unit.Value;
        }

        public async Task<Unit> Handle(DeleteTaskCommand request, CancellationToken cancellationToken)
        {
            var task = await unitOfWork.Tasks.FindById(request.Id);
            if (task == null)
            {
                throw new Exception();
            }

            await unitOfWork.Tasks.Remove(task);

            return Unit.Value;
        }

        public async Task<Unit> Handle(CreateTaskCommand request, CancellationToken cancellationToken)
        {
            var taskEntity = mapper.Map<EntityTask.Task>(request);
            await unitOfWork.Tasks.Create(taskEntity);

            return Unit.Value;
        }       
    }
}
