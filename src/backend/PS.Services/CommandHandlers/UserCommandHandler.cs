﻿using AutoMapper;
using MediatR;
using PS.Interfaces;
using PS.Services.Command.User;
using System;
using System.Threading;
using System.Threading.Tasks;
using EntityTask = PS.Domain.Entities;

namespace PS.Services.CommandHandlers
{
    public class UserCommandHandler : IRequestHandler<CreateUserCommand>, IRequestHandler<UpdateUserCommand>, IRequestHandler<DeleteUserCommand>
    {
        private readonly IMapper mapper;
        private readonly IUnitOfWork unitOfWork;
        public UserCommandHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            this.mapper = mapper;
            this.unitOfWork = unitOfWork;
        }

        public async Task<Unit> Handle(CreateUserCommand request, CancellationToken cancellationToken)
        {
            var userEntity = mapper.Map<EntityTask.User>(request);
            await unitOfWork.Users.Create(userEntity);
            await unitOfWork.Complete();

            return Unit.Value;
        }

        public async Task<Unit> Handle(UpdateUserCommand request, CancellationToken cancellationToken)
        {
            var user = await unitOfWork.Users.FindById(request.Id);
            if (user == null)
            {
                throw new Exception();
            }

            user.FirstName = request.FirstName;
            user.LastName = request.LastName;
            user.Email = request.Email;
            user.BirthDay = request.BirthDay;
            user.TeamId = request.TeamId;

            await unitOfWork.Users.Update(user);

            return Unit.Value;
        }

        public async Task<Unit> Handle(DeleteUserCommand request, CancellationToken cancellationToken)
        {
            var user = await unitOfWork.Users.FindById(request.Id);
            if (user == null)
            {
                throw new Exception();
            }

            await unitOfWork.Users.Remove(user);

            return Unit.Value;
        }     
    }
}
