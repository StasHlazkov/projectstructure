﻿using AutoMapper;
using MediatR;
using PS.Interfaces;
using PS.Services.Command.Project;
using System;
using System.Threading;
using System.Threading.Tasks;
using EntityTask = PS.Domain.Entities;

namespace PS.Services.CommandHandlers
{
    public class ProjectCommandHandler : IRequestHandler<CreateProjectCommand>, IRequestHandler<UpdateProjectCommand>, IRequestHandler<DeleteProjectCommand>
    {
        private readonly IMapper mapper;
        private readonly IUnitOfWork unitOfWork;
        public ProjectCommandHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            this.mapper = mapper;
            this.unitOfWork = unitOfWork;
        }

        public async Task<Unit> Handle(DeleteProjectCommand request, CancellationToken cancellationToken)
        {
            var project = await unitOfWork.Projects.FindById(request.Id);
            if (project == null)
            {
                throw new Exception();
            }

            await unitOfWork.Projects.Remove(project);

            return Unit.Value;
        }

        public async Task<Unit> Handle(UpdateProjectCommand request, CancellationToken cancellationToken)
        {
            var project = await unitOfWork.Projects.FindById(request.Id);
            if (project == null)
            {
                throw new Exception();
            }

            project.Name = request.Name;
            project.Description = request.Description;
            project.TeamId = request.TeamId;
            project.AuthorId = request.AuthorId;

            await unitOfWork.Projects.Update(project);

            return Unit.Value;
        }

        public async Task<Unit> Handle(CreateProjectCommand request, CancellationToken cancellationToken)
        {
            var projectEntity = mapper.Map<EntityTask.Project>(request);
            await unitOfWork.Projects.Create(projectEntity);
            await unitOfWork.Complete();

            return Unit.Value;
        }          
    }
}
