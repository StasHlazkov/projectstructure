﻿using AutoMapper;
using MediatR;
using PS.Interfaces;
using PS.Services.Command.Team;
using System;
using System.Threading;
using System.Threading.Tasks;
using EntityTask = PS.Domain.Entities;

namespace PS.Services.CommandHandlers
{
    public class TeamCommandHandler : IRequestHandler<CreateTeamCommand>, IRequestHandler<UpdateTeamCommand>, IRequestHandler<DeleteTeamCommand>
    {
        private readonly IMapper mapper;
        private readonly IUnitOfWork unitOfWork;
        public TeamCommandHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            this.mapper = mapper;
            this.unitOfWork = unitOfWork;
        }

        public async Task<Unit> Handle(CreateTeamCommand request, CancellationToken cancellationToken)
        {
            var teamEntity = mapper.Map<EntityTask.Team>(request);
            await unitOfWork.Teams.Create(teamEntity);
            await unitOfWork.Complete();

            return Unit.Value;
        }

        public async Task<Unit> Handle(UpdateTeamCommand request, CancellationToken cancellationToken)
        {
            var team = await unitOfWork.Teams.FindById(request.Id);
            if (team == null)
            {
                throw new Exception();
            }

            team.Name = request.Name;

            await unitOfWork.Teams.Update(team);

            return Unit.Value;
        }

        public async Task<Unit> Handle(DeleteTeamCommand request, CancellationToken cancellationToken)
        {
            var team = await unitOfWork.Teams.FindById(request.Id);
            if (team == null)
            {
                throw new Exception();
            }

            await unitOfWork.Teams.Remove(team);

            return Unit.Value;
        }       
    }
}
