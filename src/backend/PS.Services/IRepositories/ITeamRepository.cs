﻿using PS.Domain.Entities;
using PS.Services.IGenericRepository;

namespace PS.Services.IRepositories
{
    public interface ITeamRepository : IGenericRepository<Team>
    {
    }
}
