﻿using PS.Services.IGenericRepository;
using EntityTask = PS.Domain.Entities;

namespace PS.Services.IRepositories
{
    public interface ITaskRepository : IGenericRepository<EntityTask.Task>
    {
    }
}
