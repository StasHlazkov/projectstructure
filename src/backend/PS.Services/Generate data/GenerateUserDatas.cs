﻿using System;
using System.Collections.Generic;
using EntityTask = PS.Domain.Entities;

namespace PS.Domain.DAL.Generate_data
{
    public static class GenerateUserDatas
    {
        public static IList<EntityTask.User> GenerateData()
        {
            List<EntityTask.User> users = new List<EntityTask.User>()
            {
                new EntityTask.User()
                {
                   Id = 20,
                   TeamId = 0,
                   FirstName = "Vivian",
                   LastName = "Mertz",
                   Email = "Vivian99@yahoo.com",
                   RegisteredAt = DateTime.Now,
                   BirthDay = new DateTime(1995,7,14)
                }
            };
            return users;
        }
    }
}
