﻿using System;
using System.Collections.Generic;
using EntityTask = PS.Domain.Entities;

namespace PS.Domain.DAL.Generate_data
{
    public static class GenerateTaskDatas
    {
        public static IList<EntityTask.Task> GenerateData()
        {
            List<EntityTask.Task> tasks = new List<EntityTask.Task>()
            {
                new EntityTask.Task(){
                    Id = 0,
                    ProjectId = 0,
                    PerformerId = 23,
                    Name = "index",
                    Description = "Eveniet nihil asperiores esse minima.",
                    State = 2,
                    CreatedAt = DateTime.Now,
                    FinishedAt = null
                }
            };
            return tasks;
        }
    }
}
