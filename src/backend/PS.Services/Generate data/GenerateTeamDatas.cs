﻿using System;
using System.Collections.Generic;
using EntityTask = PS.Domain.Entities;

namespace PS.Domain.DAL.Generate_data
{
    public static class GenerateTeamDatas
    {
        public static IList<EntityTask.Team> GenerateData()
        {
            List<EntityTask.Team> teams = new List<EntityTask.Team>()
            {
               new EntityTask.Team()
               {
                   Id = 0,
                   Name = "Denesik - Greenfelder",
                   CreatedAt = new DateTime(2019,08,25)
               }
            };
            return teams;
        }
    }
}
