﻿using PS.Domain.Entities;
using System;
using System.Collections.Generic;

namespace PS.Domain.DAL.Generate_data
{
    public static class GenerateProjectDatas
    {
        public static IList<Project> GenerateDatas()
        {
            List<Project> projects = new List<Project>()
            {
                new Project()
            {
                AuthorId = 1,
                CreatedAt = DateTime.Now,
                Deadline = DateTime.Now.AddDays(3),
                Description = "Do very quickly",
                Id = 0,
                Name = "Stas",
                TeamId = 1
            },
                new Project()
            {
                AuthorId = 1,
                CreatedAt = DateTime.Now,
                Deadline = DateTime.Now.AddDays(3),
                Description = "Do very quickly",
                Id = 0,
                Name = "Stas",
                TeamId = 1
            }
            };

            return projects;
        }
    }
}
