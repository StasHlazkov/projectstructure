﻿using System.Threading.Tasks;

namespace PS.Services.Abstractions
{
    public interface IQueryProcessor
    {
        Task<TResult> ProcessAsync<TQuery, TResult>(TQuery query) where TQuery : IQuery<TResult>;
    }
}
