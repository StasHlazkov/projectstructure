﻿using System.Threading.Tasks;

namespace PS.Services.Abstractions
{
    public interface IRequestHandler<in TCommand> where TCommand :  ICommand
    {
        Task HandleAsync(TCommand command);
    }
}
