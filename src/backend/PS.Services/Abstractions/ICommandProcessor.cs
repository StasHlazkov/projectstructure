﻿using System.Threading.Tasks;

namespace PS.Services.Abstractions
{
    public interface ICommandProcessor
    {
        Task ProcessAsync<T>(T command) where T : ICommand;
    }
}
