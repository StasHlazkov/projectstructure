﻿using PS.Domain.DAL.Context;
using PS.Interfaces;
using PS.Interfaces.IRepositories;
using PS.Services.Repositories;
using System.Threading.Tasks;

namespace PS.Services.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ApplicationContext _context;
        public UnitOfWork(ApplicationContext context)
        {
            _context = context;
            Projects = new ProjectRepository(_context);
            Tasks = new TaskRepository(_context);
            Teams = new TeamRepository(_context);
            Users = new UserRepository(_context);

        }
        public IProjectRepository Projects { get; private set; }
        public ITaskRepository Tasks { get; private set; }
        public ITeamRepository Teams { get; private set; }
        public IUserRepository Users { get; private set; }

        public async Task<int> Complete()
        {
            return await _context.SaveChangesAsync();
        }
        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
