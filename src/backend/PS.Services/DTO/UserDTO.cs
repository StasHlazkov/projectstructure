﻿using PS.Domain.Entities;
using System;
using System.Collections.Generic;
using EntityTask = PS.Domain.Entities;

namespace PS.Services.DTO
{
    public class UserDTO
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime RegisteredAt { get; set; }
        public DateTime BirthDay { get; set; }
        public int? TeamId { get; set; }
    }
}
