﻿using PS.Services.IRepositories;
using System;

namespace PS.Services
{
    public interface IUnitOfWork : IDisposable
    {
        IProjectRepository Projects { get; }
        ITaskRepository Tasks { get; }
        ITeamRepository Teams { get; }
        IUserRepository Users { get; }
        int Complete();
    }
}
