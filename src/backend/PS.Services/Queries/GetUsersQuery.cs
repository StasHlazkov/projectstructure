﻿using MediatR;
using PS.Services.DTO;
using System.Collections.Generic;

namespace PS.Services.Queries
{
    public class GetUsersQuery : IRequest<IEnumerable<UserDTO>>
    {
    }
}
