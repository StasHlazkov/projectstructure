﻿using PS.Domain.DAL.Context;
using PS.Interfaces.IGenericRepository;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PS.Services.GenericPepository
{
    public class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : class
    {
        protected ApplicationContext _context;
        public GenericRepository(ApplicationContext context)
        {
            _context = context;
        }

        public async Task Create(TEntity item)
        {
            throw new NotImplementedException();
        }

        public async Task<TEntity> FindById(int id)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<TEntity>> Get()
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<TEntity>> Get(Func<TEntity, bool> predicate)
        {
            throw new NotImplementedException();
        }

        public async Task Remove(TEntity item)
        {
            throw new NotImplementedException();
        }

        public async Task Update(TEntity item)
        {
            throw new NotImplementedException();
        }
    }
}
