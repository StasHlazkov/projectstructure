﻿using AutoMapper;
using MediatR;
using PS.Interfaces;
using PS.Services.DTO;
using PS.Services.Queries;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace PS.Services.QueryHandlers
{
    public class TasksQueryHandler : IRequestHandler<GetTasksQuery, IEnumerable<TaskDTO>>
    {
        private readonly IMapper mapper;
        private readonly IUnitOfWork unitOfWork;
        public TasksQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            this.mapper = mapper;
            this.unitOfWork = unitOfWork;
        }

        public async Task<IEnumerable<TaskDTO>> Handle(GetTasksQuery request, CancellationToken cancellationToken)
        {
            return mapper.Map<IEnumerable<TaskDTO>>(await unitOfWork.Tasks.Get());
        }       
    }
}
