﻿using AutoMapper;
using MediatR;
using PS.Interfaces;
using PS.Services.DTO;
using PS.Services.Queries;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace PS.Services.QueryHandlers
{
    public class ProjectQueryHandler : IRequestHandler<GetProjectsQuery, IEnumerable<ProjectDTO>>
    {
        private readonly IMapper mapper;
        private readonly IUnitOfWork unitOfWork;
        public ProjectQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            this.mapper = mapper;
            this.unitOfWork = unitOfWork;
        }

        public async Task<IEnumerable<ProjectDTO>> Handle(GetProjectsQuery request, CancellationToken cancellationToken)
        {
            return mapper.Map<IEnumerable<ProjectDTO>>(await unitOfWork.Projects.Get());
        }      
    }
}
