﻿using AutoMapper;
using MediatR;
using PS.Interfaces;
using PS.Services.DTO;
using PS.Services.Queries;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace PS.Services.QueryHandlers
{
    public class TeamQueryHandler : IRequestHandler<GetTeamsQuery, IEnumerable<TeamDTO>>
    {
        private readonly IMapper mapper;
        private readonly IUnitOfWork unitOfWork;
        public TeamQueryHandler(IUnitOfWork unitOfWork, IMapper mapper)
        {
            this.mapper = mapper;
            this.unitOfWork = unitOfWork;
        }

        public async Task<IEnumerable<TeamDTO>> Handle(GetTeamsQuery request, CancellationToken ct)
        {
            return mapper.Map<IEnumerable<TeamDTO>>(await unitOfWork.Teams.Get());
        }
    }
}
