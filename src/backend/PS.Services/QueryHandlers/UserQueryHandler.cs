﻿using AutoMapper;
using MediatR;
using PS.Interfaces;
using PS.Services.DTO;
using PS.Services.Queries;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace PS.Services.QueryHandlers
{
    public class UserQueryHandler : IRequestHandler<GetUsersQuery, IEnumerable<UserDTO>>
    {
        private readonly IMapper mapper;
        private readonly IUnitOfWork unitOfWork;
        public UserQueryHandler(IUnitOfWork unitOfWork,IMapper mapper)
        {
            this.mapper = mapper;
            this.unitOfWork = unitOfWork;
        }

        public async Task<IEnumerable<UserDTO>> Handle(GetUsersQuery request, CancellationToken cancellationToken)
        {
            var users = await unitOfWork.Users.Get();
            return mapper.Map<IEnumerable<UserDTO>>(users);
        }      
    }
}
