﻿using AutoMapper;
using PS.Domain.Entities;
using PS.Services.Command.Team;
using PS.Services.DTO;

namespace PS.Services.MappingProfiles
{
    public class TeamProfile : Profile
    {
        public TeamProfile()
        {
            CreateMap<Team, TeamDTO>()
                .ForMember(ent => ent.Id, ctc => ctc.MapFrom(ctc => ctc.Id))
                .ForMember(ent => ent.Name, ctc => ctc.MapFrom(ctc => ctc.Name))
                .ForMember(ent => ent.CreatedAt, ctc => ctc.MapFrom(ctc => ctc.CreatedAt));

            CreateMap<CreateTeamCommand, Team>()
                .ForMember(ent => ent.Name, ctc => ctc.MapFrom(ctc => ctc.Name));

        }
    }
}
