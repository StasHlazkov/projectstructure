﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using Project_Structure.Extensions;
using System;

namespace Project_Structure.Filter
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class CustomExceptionFilterAttribute : ExceptionFilterAttribute
    {
        private readonly ILogger<CustomExceptionFilterAttribute> _logger;
        public CustomExceptionFilterAttribute(ILogger<CustomExceptionFilterAttribute> logger)
        {
            _logger = logger;
        }
        public override void OnException(ExceptionContext context)
        {
            _logger.LogError(new EventId(context.Exception.HResult),
                    context.Exception,
                    context.Exception.Message);

            var (statusCode, errorCode) = context.Exception.ParseException();

            context.HttpContext.Response.StatusCode = (int)statusCode;
            context.Result = new JsonResult(new 
            {
                ErrorMesage = context.Exception.Message,
                errorCode = errorCode
            });
        }
    }

}
