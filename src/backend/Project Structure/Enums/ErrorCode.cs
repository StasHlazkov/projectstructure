﻿namespace Project_Structure.Enums
{
    public enum ErrorCode
    {
        General = 1,
        NotFound,
        BadRequest
    }
}
