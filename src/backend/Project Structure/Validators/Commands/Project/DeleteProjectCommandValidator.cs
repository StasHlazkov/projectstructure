﻿using FluentValidation;
using PS.Services.Command.Project;

namespace Project_Structure.Validators.Commands.Project
{
    public class DeleteProjectCommandValidator : AbstractValidator<DeleteProjectCommand>
    {
        public DeleteProjectCommandValidator()
        {
            RuleFor(x => x.Id)
                    .NotEmpty()
                    .WithMessage("Id can't be empty");
        }
    }
}
