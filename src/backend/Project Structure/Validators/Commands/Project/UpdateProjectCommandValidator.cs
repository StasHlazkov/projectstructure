﻿using FluentValidation;
using PS.Services.Command.Project;

namespace Project_Structure.Validators.Commands.Project
{
    public class UpdateProjectCommandValidator : AbstractValidator<UpdateProjectCommand>
    {
        public UpdateProjectCommandValidator()
        {
            RuleFor(x => x.Id)
                   .NotEmpty()
                   .WithMessage("Id can't be empty");
            
            RuleFor(x => x.AuthorId)
                   .NotEmpty()
                   .WithMessage("AuthorId can't be empty");
           
            RuleFor(x => x.TeamId)
                   .NotEmpty()
                   .WithMessage("TeamId can't be empty");
           
            RuleFor(x => x.Name)
                   .NotEmpty()
                   .WithMessage("Name can't be empty");
           
            RuleFor(x => x.Description)
                   .NotEmpty()
                   .WithMessage("Description can't be empty");
            
            RuleFor(x => x.Deadline)
                   .NotEmpty()
                   .WithMessage("Deadline can't be empty");

        }
    }
}