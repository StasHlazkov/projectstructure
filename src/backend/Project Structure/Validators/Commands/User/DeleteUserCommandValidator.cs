﻿using FluentValidation;
using PS.Services.Command.User;

namespace Project_Structure.Validators.Commands.User
{
    public class DeleteUserCommandValidator : AbstractValidator<DeleteUserCommand>
    {
        public DeleteUserCommandValidator()
        {
            RuleFor(x => x.Id)
                   .NotEmpty()
                   .WithMessage("Id can't be empty");
        }
    }
}
