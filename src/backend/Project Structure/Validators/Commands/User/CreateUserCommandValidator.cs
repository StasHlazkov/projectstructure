﻿using FluentValidation;
using PS.Services.Command.User;

namespace Project_Structure.Validators.Commands.User
{
    public class CreateUserCommandValidator : AbstractValidator<CreateUserCommand>
    {
        public CreateUserCommandValidator()
        {

            RuleFor(x => x.TeamId)
                   .NotEmpty()
                   .WithMessage("TeamId can't be empty");

            RuleFor(x => x.FirstName)
                   .NotEmpty()
                   .WithMessage("Id can't be empty");

            RuleFor(x => x.LastName)
                   .NotEmpty()
                   .WithMessage("LastName can't be empty");

            RuleFor(x => x.Email)
                   .NotEmpty()
                   .WithMessage("Email can't be empty");

            RuleFor(x => x.RegisteredAt)
                   .NotEmpty()
                   .WithMessage("RegisteredAt can't be empty");

            RuleFor(x => x.BirthDay)
                   .NotEmpty()
                   .WithMessage("BirthDay can't be empty");
        }
    }
}