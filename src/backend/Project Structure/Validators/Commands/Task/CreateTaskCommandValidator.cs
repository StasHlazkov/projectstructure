﻿using FluentValidation;
using PS.Services.Command.Task;

namespace Project_Structure.Validators.Commands.Task
{
    public class CreateTaskCommandValidator : AbstractValidator<CreateTaskCommand>
    {
        public CreateTaskCommandValidator()
        {
            RuleFor(x => x.ProjectId)
                  .NotEmpty()
                  .WithMessage("ProjectId can't be empty");

            RuleFor(x => x.PerformerId)
                  .NotEmpty()
                  .WithMessage("PerformerId can't be empty");

            RuleFor(x => x.Name)
                  .NotEmpty()
                  .WithMessage("Name can't be empty");

            RuleFor(x => x.Description)
                  .NotEmpty()
                  .WithMessage("Description can't be empty");

            RuleFor(x => x.State)
                  .NotEmpty()
                  .WithMessage("State can't be empty");
        }
    }
}
