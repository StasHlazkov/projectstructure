﻿using FluentValidation;
using PS.Services.Command.Task;

namespace Project_Structure.Validators.Commands.Task
{
    public class DeleteTaskCommandValidator : AbstractValidator<DeleteTaskCommand>
    {
        public DeleteTaskCommandValidator()
        {
            RuleFor(x => x.Id)
                   .NotEmpty()
                   .WithMessage("Id can't be empty");
        }
    }
}
