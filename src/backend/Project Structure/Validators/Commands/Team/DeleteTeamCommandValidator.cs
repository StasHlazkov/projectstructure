﻿using FluentValidation;
using PS.Services.Command.Team;

namespace Project_Structure.Validators.Commands.Team
{
    public class DeleteTeamCommandValidator : AbstractValidator<DeleteTeamCommand>
    {
        public DeleteTeamCommandValidator()
        {
            RuleFor(x => x.Id)
                   .NotEmpty()
                   .WithMessage("Id can't be empty");
        }
    }
}
