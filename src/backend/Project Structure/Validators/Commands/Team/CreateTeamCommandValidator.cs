﻿using FluentValidation;
using PS.Services.Command.Team;

namespace Project_Structure.Validators.Commands.Team
{
    public class CreateTeamCommandValidator : AbstractValidator<CreateTeamCommand>
    {
        public CreateTeamCommandValidator()
        {
            RuleFor(x => x.Name)
                  .NotEmpty()
                  .WithMessage("Name can't be empty");
        }
    }
}
