﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using System;

namespace Project_Structure.Extensions
{
    [ApiController]
    public class BaseController : ControllerBase
    {
        internal IMediator _mediator { get; }
        public BaseController(IMediator mediator)
        {
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
        }
    }
}
