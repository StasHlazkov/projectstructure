﻿using Project_Structure.Enums;
using PS.Services.Exceptions;
using System;
using System.Net;

namespace Project_Structure.Extensions
{
    public static class ExceptionFilterExtensions
    {
        public static (HttpStatusCode statusCode, ErrorCode errorCode) ParseException(this Exception exception)
        {
            return exception switch
            {
                NotFoundException _ => (HttpStatusCode.NotFound, ErrorCode.NotFound),
                BadRequestException _ => (HttpStatusCode.BadRequest, ErrorCode.BadRequest),
                _ => (HttpStatusCode.InternalServerError, ErrorCode.General),
            };
        }
    }
}
