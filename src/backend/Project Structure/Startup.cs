using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using Project_Structure.Extensions;
using Project_Structure.Filter;
using PS.Domain.DAL.Context;
using PS.Services.GenericPepository;
using PS.Services.UnitOfWork;
using System.Reflection;

namespace Project_Structure
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var migrationAssembly = typeof(ApplicationContext).Assembly.GetName().Name;
            services.AddDbContext<ApplicationContext>(option =>
            {
                option.UseSqlServer(Configuration["ConnectionStrings:ProjectStructuraDBConnection"], opt => opt.MigrationsAssembly(migrationAssembly));
            });
            services.RegisterAutoMapper();
            services.CreateRepository();

            services.AddMediatR(typeof(UnitOfWork).GetTypeInfo().Assembly);

            services.AddControllers(options => options.Filters.Add(typeof(CustomExceptionFilterAttribute)));
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Project_Structure", Version = "v1" });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Project_Structure v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            InitializeDatabase(app);
        }
        private static void InitializeDatabase(IApplicationBuilder app)
        {
            using (var scope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
            {
                using var context = scope.ServiceProvider.GetRequiredService<ApplicationContext>();
                context.Database.Migrate();
            };
        }

    }
}
