﻿using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Project_Structure.Extensions;
using PS.Services.Command.Task;
using PS.Services.DTO;
using PS.Services.Queries;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Project_Structure.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TaskController : BaseController
    {
        public TaskController(IMediator mediator) : base(mediator) { }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<TaskDTO>))]
        public async Task<ActionResult> GetAllTasks(CancellationToken ct)
        {
            GetTasksQuery getProjects = new GetTasksQuery();
            var projects = await _mediator.Send(getProjects, ct);
            return Ok(projects);
        }
        [HttpPost("create")]

        public async Task<ActionResult> CreateTask(CancellationToken ct, [FromBody] CreateTaskCommand createProject)
        {
            await _mediator.Send(createProject, ct);
            return Ok();
        }

        [HttpDelete("delete")]

        public async Task<ActionResult> RemoveTask(CancellationToken ct, [FromBody] DeleteTaskCommand deleteProject)
        {
            await _mediator.Send(deleteProject, ct);
            return Ok();
        }

        [HttpPut("update")]

        public async Task<ActionResult> UpdateTask(CancellationToken ct, [FromBody] UpdateTaskCommand updateProject)
        {
            await _mediator.Send(updateProject, ct);
            return Ok();
        }
    }
}
