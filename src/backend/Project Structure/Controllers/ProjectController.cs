﻿using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Project_Structure.Extensions;
using PS.Services.Command.Project;
using PS.Services.DTO;
using PS.Services.Queries;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Project_Structure.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectController : BaseController
    {
        public ProjectController(IMediator mediator) : base(mediator) { }


        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<ProjectDTO>))]
        public async Task<ActionResult> GetAllProjects(CancellationToken ct)
        {
            GetProjectsQuery getProjects = new GetProjectsQuery();
            var projects = await _mediator.Send(getProjects, ct);
            return Ok(projects);
        }

        [HttpPost("create")]       
        public async Task<ActionResult> CreateProject(CancellationToken ct, [FromBody] CreateProjectCommand createProject)
        {
            await _mediator.Send(createProject, ct);
            return Ok();
        }

        [HttpDelete("delete")]
        public async Task<ActionResult> RemoveProject(CancellationToken ct, [FromBody] DeleteProjectCommand deleteProject)
        {
            await _mediator.Send(deleteProject, ct);
            return Ok();
        }

        [HttpPut("update")]
        public async Task<ActionResult> UpdateProject(CancellationToken ct, [FromBody] UpdateProjectCommand updateProject)
        {
            await _mediator.Send(updateProject, ct);
            return Ok();
        }
    }
}
