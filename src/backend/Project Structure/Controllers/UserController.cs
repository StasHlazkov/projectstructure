﻿using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Project_Structure.Extensions;
using PS.Services.Command.User;
using PS.Services.DTO;
using PS.Services.Queries;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Project_Structure.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : BaseController
    {
        public UserController(IMediator mediator) : base(mediator) { }


        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<UserDTO>))]
        public async Task<ActionResult> GetAllTasks(CancellationToken ct)
        {
            GetUsersQuery getProjects = new GetUsersQuery();
            var projects = await _mediator.Send(getProjects, ct);
            return Ok(projects);
        }

        [HttpPost("create")]
        public async Task<ActionResult> CreateUser(CancellationToken ct ,[FromBody] CreateUserCommand createProject)
        {
            await _mediator.Send(createProject, ct);
            return Ok();
        }

        [HttpDelete("delete")]
        public async Task<ActionResult> RemoveUser(CancellationToken ct, [FromBody] DeleteUserCommand deleteProject)
        {
            await _mediator.Send(deleteProject, ct);
            return Ok();
        }

        [HttpPut("update")]
        public async Task<ActionResult> UpdateUser(CancellationToken ct, [FromBody] UpdateUserCommand updateProject)
        {
            await _mediator.Send(updateProject, ct);
            return Ok();
        }
    }
}
