﻿using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Project_Structure.Extensions;
using PS.Services.Command.Team;
using PS.Services.DTO;
using PS.Services.Queries;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Project_Structure.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamController : BaseController
    {
        public TeamController(IMediator mediator) : base(mediator) { }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<TeamDTO>))]
        public async Task<ActionResult> GetAllTeams(CancellationToken ct)
        {
            GetTeamsQuery getProjects = new GetTeamsQuery();
            var projects = await _mediator.Send(getProjects, ct);
            return Ok(projects);
        }

        [HttpPost("create")]
        public async Task<ActionResult> CreateTeam(CancellationToken ct,[FromBody] CreateTeamCommand createProject)
        {
            await _mediator.Send(createProject, ct);
            return Ok();
        }

        [HttpDelete("delete")]
        public async Task<ActionResult> RemoveTeam(CancellationToken ct, [FromBody] DeleteTeamCommand deleteProject)
        {
            await _mediator.Send(deleteProject, ct);
            return Ok();
        }

        [HttpPut("update")]
        public async Task<ActionResult> UpdateTeam(CancellationToken ct, [FromBody] UpdateTeamCommand updateProject)
        {
            await _mediator.Send(updateProject, ct);
            return Ok();
        }
    }
}
