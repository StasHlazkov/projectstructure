﻿using PS.Interfaces.IRepositories;
using System;
using System.Threading.Tasks;

namespace PS.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IProjectRepository Projects { get; }
        ITaskRepository Tasks { get; }
        ITeamRepository Teams { get; }
        IUserRepository Users { get; }
        Task<int> Complete();
    }
}
