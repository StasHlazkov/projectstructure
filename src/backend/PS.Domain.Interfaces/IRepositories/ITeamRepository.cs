﻿using PS.Domain.Entities;
using PS.Interfaces.IGenericRepository;

namespace PS.Interfaces.IRepositories
{
    public interface ITeamRepository : IGenericRepository<Team>
    {
    }
}
