﻿using PS.Interfaces.IGenericRepository;
using EntityTask = PS.Domain.Entities;

namespace PS.Interfaces.IRepositories
{
    public interface ITaskRepository : IGenericRepository<EntityTask.Task>
    {
    }
}
